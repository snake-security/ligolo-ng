rm -rf /opt/ANDRAX/ligolo

mkdir /opt/ANDRAX/ligolo

go build -o ligolo-ng cmd/proxy/main.go

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Go build proxy... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip ligolo-ng

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip ligolo-ng... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf ligolo-ng /opt/ANDRAX/ligolo/

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy ligolo-ng... PASS!"
else
  # houston we have a problem
  exit 1
fi

GOOS=windows GOARCH=amd64 go build -o ligolo-agent.exe cmd/agent/main.go

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Go build windows amd64 agent... PASS!"
else
  # houston we have a problem
  exit 1
fi

GOOS=darwin GOARCH=amd64 go build -o ligolo-agent-darwin cmd/agent/main.go

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Go build Darwin amd64 agent... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf ligolo-agent-darwin /opt/ANDRAX/ligolo/

if [ $(uname -m | grep 'x86_64') ]; then
   GOOS=linux CC=x86_64-linux-gnu-gcc go build -o ligolo-agent-linux cmd/agent/main.go
   x86_64-linux-gnu-strip ligolo-agent-linux
else
   GOOS=linux GOARCH=amd64 CC=x86_64-multilib-linux-gnu-gcc go build -o ligolo-agent-linux cmd/agent/main.go
   x86_64-multilib-linux-gnu-strip ligolo-agent-linux
fi

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "go build Linux amd64 agent... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf ligolo-agent-linux /opt/ANDRAX/ligolo/

GOOS=linux GOARCH=arm CC=arm-linux-gnueabihf-gcc go build -o ligolo-agent-arm cmd/agent/main.go
arm-linux-gnueabihf-strip ligolo-agent-arm

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Go build Linux arm agent... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf ligolo-agent-arm /opt/ANDRAX/ligolo/

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
